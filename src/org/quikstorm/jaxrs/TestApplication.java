package org.quikstorm.jaxrs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider;
import com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON;
import com.wordnik.swagger.jaxrs.listing.ResourceListingProvider;

@ApplicationPath("/api")
public class TestApplication extends Application{

//	public TestApplication(){
//		packages("org.quikstorm.jaxrs", "org.codehaus.jackson.jaxrs", "com.wordnik.swagger.jersey.listing");
//	}
	
	@Override
	public Set<Object> getSingletons() {
		Set<Object> objects = new HashSet<Object>();
		objects.add(new ServiceProviderKeyResource());
		objects.add(new JacksonJsonProvider());
		// Swagger Objects
		objects.add(new ApiDeclarationProvider());
		objects.add(new ResourceListingProvider());
		objects.add(new ApiListingResourceJSON());
		return objects;
	}
	
	
}
