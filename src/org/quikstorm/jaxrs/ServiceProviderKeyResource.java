package org.quikstorm.jaxrs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/ServiceProviderKey")
@Api(value="/ServiceProviderKey", description="Operations about ServiceProviderKey objects")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class ServiceProviderKeyResource {

	private Map<String,ServiceProviderKey> keys = new HashMap<String,ServiceProviderKey>();
	
	public ServiceProviderKeyResource(){
		for ( int i = 0; i < 10; ++i ){
			ServiceProviderKey k = new ServiceProviderKey();
			k.setName("Key_" + i);
			k.setPublicKey("the_public_key");
			keys.put(k.getName(), k);
		}
	}
	
	@GET
	@ApiOperation(value = "List ServiceProviderKeys", position=0, response=ServiceProviderKeys.class)
	public ServiceProviderKeys list(){
		ServiceProviderKeys rval = new ServiceProviderKeys();
		rval.setKeys(new ArrayList<ServiceProviderKey>(keys.values()));
		return rval;
	}
	
	@GET
	@Path("/{name}")
	@ApiOperation(value="Get ServiceProviderKey", position=1, response=ServiceProviderKey.class)
	@ApiResponses({
		@ApiResponse(code=404, message="Not Found")
	})
	public ServiceProviderKey get(@PathParam("name") String name){
		ServiceProviderKey key = keys.get(name);
		if ( key != null ){
			return key;
		} else {
			throw new WebApplicationException(Response.status(Status.NOT_FOUND).type(MediaType.TEXT_PLAIN).entity("ServiceProviderKey#" + name + " was not found.").build());
		}
	}
	
	@PUT
	@Path("/{name}")
	@ApiOperation(value="Update a ServiceProviderKey", response=ServiceProviderKey.class, position=2)
	public ServiceProviderKey update(@PathParam("name") String name, ServiceProviderKey newKey){
		ServiceProviderKey key = get(name);
		key.setName(newKey.getName());
		key.setPublicKey(newKey.getPublicKey());
		return key;
	}
	
	@DELETE
	@Path("/{name}")
	@ApiOperation(value="Delete a ServiceProviderKey", response=ServiceProviderKey.class, position=3)
	public ServiceProviderKey delete(@PathParam("name") String name){
		ServiceProviderKey key = get(name);
		keys.remove(name);
		return key;
	}
	
	@GET
	@Path("/{name}/publicKey")
	@ApiOperation(value="Retrieve only the public key value.", response=Result.class, position=4)
	public Result getPublicKey(@PathParam("name") String name){
		return new Result(get(name).getPublicKey());
	}
	
}
