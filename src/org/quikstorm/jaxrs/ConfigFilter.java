package org.quikstorm.jaxrs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.wordnik.swagger.config.ConfigFactory;
import com.wordnik.swagger.config.ScannerFactory;
import com.wordnik.swagger.config.SwaggerConfig;
import com.wordnik.swagger.jaxrs.config.DefaultJaxrsScanner;
import com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader;
import com.wordnik.swagger.reader.ClassReaders;

@WebFilter("/swagger/index.html")
public class ConfigFilter implements Filter{

	boolean wasSet = false;
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		
		if ( req instanceof HttpServletRequest ){
			HttpServletRequest httpReq = (HttpServletRequest) req;
			
			String baseUrl = (httpReq.isSecure() ? "https" : "http" ) + "://" + httpReq.getServerName() + ":" + httpReq.getServerPort() + httpReq.getContextPath() + "/api";

			if (!wasSet) {
				SwaggerConfig swaggerConfig = new SwaggerConfig();
				swaggerConfig.setApiVersion(getVersion(httpReq));
				swaggerConfig.setBasePath(baseUrl);
				ConfigFactory.setConfig(swaggerConfig);
				wasSet = true;
			} else {
				ConfigFactory.config().setBasePath(baseUrl);
			}
			
			System.out.println("updated swagger config with baseUrl=" + baseUrl);
		}
		
		chain.doFilter(req, resp);
	}
	
	private String getVersion(HttpServletRequest req){
		try{
			String path = req.getServletContext().getRealPath("version.txt");
			BufferedReader br = new BufferedReader(new FileReader(path));
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();
	
		        while (line != null) {
		            sb.append(line);
		            sb.append('\n');
		            line = br.readLine();
		        }
		        String everything = sb.toString();
		        
		        return everything;
		    } finally {
		        br.close();
		    }
		    
	    
		} catch (Exception e){
			return "Unknown_Version";
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

		ScannerFactory.setScanner(new DefaultJaxrsScanner());
		
		ClassReaders.setReader((new DefaultJaxrsApiReader()));
	}

}
