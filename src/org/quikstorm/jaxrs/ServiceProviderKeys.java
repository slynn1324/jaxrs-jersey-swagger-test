package org.quikstorm.jaxrs;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;

@XmlRootElement
public class ServiceProviderKeys implements Serializable{

	private List<ServiceProviderKey> keys;

	public List<ServiceProviderKey> getKeys() {
		return keys;
	}

	public void setKeys(List<ServiceProviderKey> keys) {
		this.keys = keys;
	}
	
	
}
