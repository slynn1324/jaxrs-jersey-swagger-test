package org.quikstorm.jaxrs;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Result wrapper class used to wrap single string response value types.
 * 
 * @author slynn1324
 *
 */
@XmlRootElement
public class Result implements Serializable{
	
	public static final Result OK = new Result("OK");
	
	private static final long serialVersionUID = 4506177954742004509L;
	
	private String value;
	
	public Result(String value){
		this.value = value;
	}
	
	protected Result(){}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	

}
